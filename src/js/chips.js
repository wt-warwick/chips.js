$.fn.chips = function chips() {
	var self = this;

	var currentColor;
	var labels = [];
	var nextId = 0;

	function __construct() {
	}

	self.chips = function add(labels) {
		var chip;
		var i;

		if (!Array.isArray(labels)) {
			labels = [labels];
		}

		for (i = 0; i < labels.length; i++) {
			chips_(labels[i]);
		}
	};

	function chips_(label) {
		var id = nextId++;

		chip = $(chipHTML(label))
		chip.data('id', id);
		chip.find('.x').click(function() {
			self.remove(id);
		});
		self.append(chip);
	}

	function chipHTML(label) {
		return '<span class="chip" style="background:' + currentColor + '">' + label + '<i class="fa fa-close x"></i></span>';
	}

	self.clear = function clear() {
		self.getElements().remove();
	};

	self.color = function color(c) {
		currentColor = c;
	};

	self.getElement = function getElement(id) {
		return self.getElements().filter(function(index, element) {
			return $(element).data('id') === id;
		});
	}

	self.getElements = function getElements() {
		return self.find('.chip');
	}

	self.unchip = function removeChip(id) {
		self.getElement(id).remove();
	};

	__construct();
	return this;
}